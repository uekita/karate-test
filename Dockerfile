FROM maven:3.6.3-jdk-8

# Set working directory
WORKDIR /app/

RUN apt-get update
RUN apt-get install -y curl
RUN apt-get install -y --no-install-recommends apt-utils
RUN apt-get install -y wget \
        build-essential \
        libgl1-mesa-glx \
        libgtk-3-dev \
        libxdamage1 \
        libdbus-glib-1-2 \
        libxt6

#install firefox
#RUN wget -P /usr/local http://ftp.mozilla.org/pub/firefox/releases/72.0.2/linux-x86_64/en-US/firefox-72.0.2.tar.bz2
#RUN tar -jxvf /usr/local/firefox-72.0.2.tar.bz2 -C /usr/local/
#RUN ln -s /usr/local/firefox/firefox /usr/bin/firefox

#install geckdriver
#RUN wget https://github.com/mozilla/geckodriver/releases/download/v0.26.0/geckodriver-v0.26.0-linux64.tar.gz
#RUN tar xvzf geckodriver-v0.26.0-linux64.tar.gz
#RUN chmod +x geckodriver
#RUN mv geckodriver /usr/local/bin/geckodriver

#install chrome
RUN wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -
RUN sh -c 'echo "deb http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list'
RUN apt-get update
RUN apt-get install -y google-chrome-stable

COPY pom.xml /app/
RUN mvn clean install



COPY . /app/

CMD ["sh", "-c", "./start-test.sh"]