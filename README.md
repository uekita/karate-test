
# Karate POC

## How to run (docker)
docker run --name karate --rm -p 5900:5900 -e KARATE_SOCAT_START=true --cap-add=SYS_ADMIN -v "$PWD":/src -v "$HOME"/.m2:/root/.m2 ptrthomas/karate-chrome && docker exec -it -w /src karate mvn clean test -Dkarate.env=docker
