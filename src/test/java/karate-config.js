function fn() {
  var env = karate.env; // get system property 'karate.env'
  karate.log('karate.env system property was:', env);
  if (!env) {
    env = 'dev';
  }
  var config = {
    env: env,
    myVarName: 'someValue'
  }
  if (env == 'docker') {
    var driverConfig = {
                type: 'chrome',
                showDriverLog: true,
                start: false,
                beforeStart: 'supervisorctl start ffmpeg',
                afterStop: 'supervisorctl stop ffmpeg',
                videoFile: '/tmp/karate.mp4'
            };
            karate.configure('driver', driverConfig);
  } else if (env == 'e2e') {
    //
  }
  return config;
}